import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
plugins {
    antlr
    idea
    java
    kotlin("jvm") version "1.4.0"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
    maven("https://jitpack.io")
}

dependencies {
    implementation(kotlin("stdlib"))
    testImplementation(kotlin("test-junit5"))
    testImplementation("org.junit.jupiter:junit-jupiter:5.6.2")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.6.2")
    antlr("org.antlr:antlr4:4.7.1")
}

tasks {
    withType<KotlinCompile>().all {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
    compileKotlin {
        dependsOn(generateGrammarSource)
    }
    test {
        useJUnitPlatform()
    }
}