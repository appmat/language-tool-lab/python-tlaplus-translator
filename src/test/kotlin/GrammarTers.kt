package ru.appmat.lambda_calculus.grammar;
import TestPropertyFile.Translator
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class GrammarTest {
    @Test
    fun operations(){
        val input = """
            i = 1;
            i = j;
            i += 1;
            i -= 1;
            i < 1;
            i > 1;
            i = a + b - c + d - e;
            i = 4 * 3;
        """.trimIndent()
        val expectation = """
            |i := 1;
            |i := j;
            |i := i + 1;
            |i := i - 1;
            |i < 1;
            |i > 1;
            |i := a + b - c + d - e;
            |i := 4 * 3;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `simple if`() {
        val input = """
            if (i < n):
                a = 1;
        """.trimIndent()
        val expectation = """
            |if (i < n) then
            |    a := 1;
            |end if;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `simple if-elif`() {
        val input = """
            if i < n:
                a = 1;
            elif i > n:
                a = 2;
        """.trimIndent()
        val expectation = """
            |if i < n then
            |    a := 1;
            |elif i > n then
            |    a := 2;
            |end if;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `simple if-elif-else`() {
        val input = """
            if i < n:
                a = 1;
            elif i > n:
                a = 2;
            else:
                a = 3;
        """.trimIndent()
        val expectation = """
            |if i < n then
            |    a := 1;
            |elif i > n then
            |    a := 2;
            |else
            |    a := 3;
            |end if;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `simple for i in range(n)`() {
        val input = """
            for i in range(10):
                a += 1;
                b -= 2;
        """.trimIndent()
        val expectation = """
            |i := 0;
            |while i < 10 do
            |    a := a + 1;
            |    b := b - 2;
            |    i := i + 1;
            |end while;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `simple for i in range(init, maxVal)`() {
        val input = """
            for i in range(1, 10):
                a += 1;
                b -= 2;
        """.trimIndent()
        val expectation = """
            |i := 1;
            |while i < 10 do
            |    a := a + 1;
            |    b := b - 2;
            |    i := i + 1;
            |end while;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `simple for i in range(init, maxVal, step)`() {
        val input = """
            for i in range(1, 10, 3):
                a += 1;
                b -= 2;
        """.trimIndent()
        val expectation = """
            |i := 1;
            |while i < 10 do
            |    a := a + 1;
            |    b := b - 2;
            |    i := i + 3;
            |end while;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `for in for`() {
        val input = """
            for i in range(1, 10, 3):
                for j in range(5):
                    a += 1;
                b -= 2;
            c = 5;
        """.trimIndent()
        val expectation = """
            |i := 1;
            |while i < 10 do
            |    j := 0;
            |    while j < 5 do
            |        a := a + 1;
            |        j := j + 1;
            |    end while;
            |    b := b - 2;
            |    i := i + 3;
            |end while;
            |c := 5;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `if-else in for`() {
        val input = """
            for i in range(10):
                if (i < 5):
                    a += 1;
                else:
                    a -= 2;
        """.trimIndent()
        val expectation = """
            |i := 0;
            |while i < 10 do
            |    if (i < 5) then
            |        a := a + 1;
            |    else
            |        a := a - 2;
            |    end if;
            |    i := i + 1;
            |end while;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `if-elif-else in for`() {
        val input = """
            for i in range(10):
                if i < n:
                    a = 1;
                elif i > n:
                    a = 2;
                else:
                    a = 3;
        """.trimIndent()
        val expectation = """
            |i := 0;
            |while i < 10 do
            |    if i < n then
            |        a := 1;
            |    elif i > n then
            |        a := 2;
            |    else
            |        a := 3;
            |    end if;
            |    i := i + 1;
            |end while;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `if with a double condition`() {
        val input = """
            if (i > 5 and i <= 10):
                a = 2;
        """.trimIndent()
        val expectation = """
            |if (i > 5 and i <= 10) then
            |    a := 2;
            |end if;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }

    @Test
    fun `for in if-elif-else`() {
        val input = """
            if (i < n):
                for i in range(1, 10, 1):
                    a += 1;
            elif (i > n):
                for i in range(1, 10, 2):
                    a += 2;
            else:
                for i in range(1, 10, 3):
                    a += 3;
        """.trimIndent()
        val expectation = """
            |if (i < n) then
            |    i := 1;
            |    while i < 10 do
            |        a := a + 1;
            |        i := i + 1;
            |    end while;
            |elif (i > n) then
            |    i := 1;
            |    while i < 10 do
            |        a := a + 2;
            |        i := i + 2;
            |    end while;
            |else
            |    i := 1;
            |    while i < 10 do
            |        a := a + 3;
            |        i := i + 3;
            |    end while;
            |end if;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `simple while`() {
        val input = """
            i = 0;
            n = 10;
            while i < n:
                n -= 1;
                i += 1;
        """.trimIndent()
        val expectation = """
            |i := 0;
            |n := 10;
            |while i < n do
            |    n := n - 1;
            |    i := i + 1;
            |end while;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }

    @Test
    fun `while in while`() {
        val input = """
            i = 0;
            while i < n:
                j = 0;
                while j < m:
                    a += 1;
                    j += 1;
                i += 1;
        """.trimIndent()
        val expectation = """
            |i := 0;
            |while i < n do
            |    j := 0;
            |    while j < m do
            |        a := a + 1;
            |        j := j + 1;
            |    end while;
            |    i := i + 1;
            |end while;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }

    @Test
    fun `while in if-elif-else`() {
        val input = """
            if i < 10:
                j = 0;
                while j < n:
                    a += 1;
                    j += 1;
            elif i > 10:
                j = 0;
                while j < n:
                    a += 2;
                    j += 1;
            else:
                j = 0;
                while j < n:
                    a += 3;
                    j += 1;
        """.trimIndent()
        val expectation = """
            |if i < 10 then
            |    j := 0;
            |    while j < n do
            |        a := a + 1;
            |        j := j + 1;
            |    end while;
            |elif i > 10 then
            |    j := 0;
            |    while j < n do
            |        a := a + 2;
            |        j := j + 1;
            |    end while;
            |else
            |    j := 0;
            |    while j < n do
            |        a := a + 3;
            |        j := j + 1;
            |    end while;
            |end if;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `while if-else`() {
        val input = """
            i = 0;
            while i < n:
                if (i < 10):
                    a = 1;
                else:
                    a = 2;
                i += 1;
        """.trimIndent()
        val expectation = """
            |i := 0;
            |while i < n do
            |    if (i < 10) then
            |        a := 1;
            |    else
            |        a := 2;
            |    end if;
            |    i := i + 1;
            |end while;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `for in while`() {
        val input = """
            i = 0;
            while i < n:
                for j in range(10):
                    a += 1;
                i += 1;
        """.trimIndent()
        val expectation = """
            |i := 0;
            |while i < n do
            |    j := 0;
            |    while j < 10 do
            |        a := a + 1;
            |        j := j + 1;
            |    end while;
            |    i := i + 1;
            |end while;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }

    @Test
    fun `while in for`() {
        val input = """
            for j in range(10):
                i = 0;
                while i < n:
                    a += 1;
                    i += 1;
        """.trimIndent()
        val expectation = """
            |j := 0;
            |while j < 10 do
            |    i := 0;
            |    while i < n do
            |        a := a + 1;
            |        i := i + 1;
            |    end while;
            |    j := j + 1;
            |end while;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `if for else while`() {
        val input = """
            if i < n:
                for j in range(10):
                    a += 1;
            else:
                j = 0;
                while j < 10:
                    a += 2;
                    j += 1;
        """.trimIndent()
        val expectation = """
            |if i < n then
            |    j := 0;
            |    while j < 10 do
            |        a := a + 1;
            |        j := j + 1;
            |    end while;
            |else
            |    j := 0;
            |    while j < 10 do
            |        a := a + 2;
            |        j := j + 1;
            |    end while;
            |end if;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `function()`() {
        val input = """
            def func():
                a = 5;
        """.trimIndent()
        val expectation = """
            |func:
            |    a := 5;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `function(param)`() {
        val input = """
            def func(a, b, c):
                a = b * c;
        """.trimIndent()
        val expectation = """
            |func:
            |    a := b * c;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `function() if-elif-else`() {
        val input = """
            def func(a):
                if i < n:
                    a = 1;
                elif i > n:
                    a = 2;
                else:
                    a = 3;
                print (a);
        """.trimIndent()
        val expectation = """
            |func:
            |    if i < n then
            |        a := 1;
            |    elif i > n then
            |        a := 2;
            |    else
            |        a := 3;
            |    end if;
            |    print (a);
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `function() for in if-elif-else`() {
        val input = """
            def func():
                if (i < n):
                    for i in range(1, 10, 1):
                        a += 1;
                elif (i > n):
                    for i in range(1, 10, 2):
                        a += 2;
                else:
                    for i in range(1, 10, 3):
                        a += 3;
        """.trimIndent()
        val expectation = """
            |func:
            |    if (i < n) then
            |        i := 1;
            |        while i < 10 do
            |            a := a + 1;
            |            i := i + 1;
            |        end while;
            |    elif (i > n) then
            |        i := 1;
            |        while i < 10 do
            |            a := a + 2;
            |            i := i + 2;
            |        end while;
            |    else
            |        i := 1;
            |        while i < 10 do
            |            a := a + 3;
            |            i := i + 3;
            |        end while;
            |    end if;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `function() while in if-elif-else`() {
        val input = """
            def func():
                if i < 10:
                    j = 0;
                    while j < n:
                        a += 1;
                        j += 1;
                elif i > 10:
                    j = 0;
                    while j < n:
                        a += 2;
                        j += 1;
                else:
                    j = 0;
                    while j < n:
                        a += 3;
                        j += 1;
        """.trimIndent()
        val expectation = """
            |func:
            |    if i < 10 then
            |        j := 0;
            |        while j < n do
            |            a := a + 1;
            |            j := j + 1;
            |        end while;
            |    elif i > 10 then
            |        j := 0;
            |        while j < n do
            |            a := a + 2;
            |            j := j + 1;
            |        end while;
            |    else
            |        j := 0;
            |        while j < n do
            |            a := a + 3;
            |            j := j + 1;
            |        end while;
            |    end if;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `function() for`() {
        val input = """
            def func():
                for i in range(10):
                    a += 1;
                    b -= 2;
        """.trimIndent()
        val expectation = """
            |func:
            |    i := 0;
            |    while i < 10 do
            |        a := a + 1;
            |        b := b - 2;
            |        i := i + 1;
            |    end while;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `function() while`() {
        val input = """
            def func():
                i = 0;
                n = 10;
                while i < n:
                    n -= 1;
                    i += 1;
        """.trimIndent()
        val expectation = """
            |func:
            |    i := 0;
            |    n := 10;
            |    while i < n do
            |        n := n - 1;
            |        i := i + 1;
            |    end while;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `class with func with while`() {
        val input = """
            class Name:
                i = 0;
                def func():
                    i = 0;
                    while i < 10:
                        n -= 1;
                        i += 1;
        """.trimIndent()
        val expectation = """
            |Name:
            |    i := 0;
            |    func:
            |        i := 0;
            |        while i < 10 do
            |            n := n - 1;
            |            i := i + 1;
            |        end while;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
    @Test
    fun `class with init`() {
        val input = """
            class Name:
                def __init__(self):
                    i = a;
                    j = b;
        """.trimIndent()
        val expectation = """
            |Name:
            |    variables i = a,
            |    j = b;
            |
            """.trimMargin()
        var output = Translator()
        assertEquals(expectation, output.Translate(input))
    }
}