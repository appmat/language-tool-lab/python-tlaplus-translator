import org.antlr.v4.misc.OrderedHashMap
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.Token;

/*
fun main() {
    val fromString = CharStreams.fromString(
        """x = 0 
           for i in range (1, 5):
                x += i""".trimIndent()
    )
    val lambdaLexer = Python3Lexer(fromString)
    val lambdaParser = Python3Parser(CommonTokenStream(lambdaLexer))
    val program = lambdaParser

    // println(program.toStringTree(lambdaParser))
}*/
//    val assignments = program.assignment()
//    val term = program.term()
////    println(assignments)
//    println("Assignments")
//    assignments.forEach { println("${it.text}: ${it.toStringTree(lambdaParser)}") }
//    println("Terms")
//    println(term.toStringTree(lambdaParser))
//    assignments.forEach {
//        it.toModel()
//    }
//    LambdaInterpreter(
//        LambdaContext.NamedTermContext(
//
//        )
//    )


/*sealed class LambdaTerm {
    abstract fun getFreeVariables(): Set<String>
    data class Symbol(val id: String) : LambdaTerm() {
        override fun getFreeVariables(): Set<String> {
            return setOf(id)
        }
    }

    data class Application(val first: LambdaTerm, val second: LambdaTerm) : LambdaTerm() {
        override fun getFreeVariables(): Set<String> {
            return first.getFreeVariables() + second.getFreeVariables()
        }
    }

    data class Function(val arguments: List<String>, val body: LambdaTerm) : LambdaTerm() {
        override fun getFreeVariables(): Set<String> {
            return body.getFreeVariables() - arguments
        }
    }
}*/


//fun LambdaParser.TermContext.toModel(): LambdaTerm {
//    println("$altNumber: $text")
////    when (this.altNumber) {
////    }
//    return LambdaTerm.Symbol("x")
//}
//
//fun LambdaParser.AssignmentContext.toModel(): Assignment {
//    return Assignment(this.variable().text, this.term().toModel())
//}

/*class Assignment(id: String, term: LambdaTerm)

sealed class LambdaContext {
    abstract fun getTerm(id: String): LambdaTerm
    abstract fun store(id: String, term: LambdaTerm)
    abstract fun canStore(id: String): Boolean
    abstract fun has(id: String): Boolean

    class NamedTermContext(
        private val context: MutableMap<String, LambdaTerm> = mutableMapOf()
    ) : LambdaContext() {

        override fun getTerm(id: String): LambdaTerm {
            return context.getValue(id)
        }

        override fun store(id: String, term: LambdaTerm) {
            context[id] = term
        }

        override fun canStore(id: String): Boolean {
            return true
        }

        override fun has(id: String): Boolean {
            return context.containsKey(id)
        }
    }

    class StaticContext(private val context: LambdaContext) : LambdaContext() {

        override fun getTerm(id: String): LambdaTerm {
            return context.getTerm(id)
        }

        override fun store(id: String, term: LambdaTerm) {
            throw IllegalStateException()
        }

        override fun canStore(id: String): Boolean {
            return false
        }

        override fun has(id: String): Boolean {
            return context.has(id)
        }
    }
}

class LambdaInterpreter(val context: LambdaContext) {

    fun interpret(term: LambdaTerm) {
        val freeVariables = term.getFreeVariables()
            .map { context.getTerm(it) }
//        interpret(term, freeVariables)
    }

    fun interpret(assignment: Assignment) {
    }
}*/
