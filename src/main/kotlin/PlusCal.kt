interface PlusCalStatement {
    fun toPlusCal(): String = error("This Statement should not be translate toPlusCal")
}

object EmptyStatement : PlusCalStatement {
    override fun toPlusCal() = ""
}

object Expr_stmtBegin : PlusCalStatement {}
object ExprBegin : PlusCalStatement {}
object AtomBegin : PlusCalStatement {}
object ExprlistBegin : PlusCalStatement {}
object TestlistBegin : PlusCalStatement {}
object IfBegin : PlusCalStatement {}
object TestBegin : PlusCalStatement {}
object TfpdefBegin : PlusCalStatement {}
object TypedargslistBegin : PlusCalStatement {}
object ParametersBegin : PlusCalStatement {}
object FuncdefBegin : PlusCalStatement {}
object SuiteBegin : PlusCalStatement{}
object SimpleStmtBegin : PlusCalStatement{}
object SmallStmtBegin : PlusCalStatement{}
object StmtBegin : PlusCalStatement{}
object CompoundStmtBegin : PlusCalStatement{}
object Or_testBegin : PlusCalStatement{}
object And_testBegin : PlusCalStatement{}
object Not_testBegin : PlusCalStatement{}
object ComparisonBegin : PlusCalStatement{}
object Comp_opBegin : PlusCalStatement{}
object Xor_exprBegin : PlusCalStatement{}
object And_exprBegin : PlusCalStatement{}
object Shift_exprBegin : PlusCalStatement{}
object Arith_exprBegin : PlusCalStatement{}
object TermBegin : PlusCalStatement{}
object FactorBegin : PlusCalStatement{}
object PowerBegin : PlusCalStatement{}
object Atom_exprBegin : PlusCalStatement{}
object TrailerBegin : PlusCalStatement{}
object ArglistBegin : PlusCalStatement{}
object SubscriptBegin : PlusCalStatement{}
object SubscriptlistBegin : PlusCalStatement{}
object Star_exprBegin : PlusCalStatement{}
object Testlist_compBegin : PlusCalStatement{}
object Comp_forBegin : PlusCalStatement{}
object WhileBegin : PlusCalStatement{}
object AnnassignBegin : PlusCalStatement{}
object AugassignBegin : PlusCalStatement{}
object ClassdefBegin : PlusCalStatement{}

val tab = "    "

fun  splitStringFor(
    str: String
): List<String>{
    //var str = s.filter { !it.isWhitespace() }
    var s = str.replace(" ", "")
    val listParam: List<String> = s.split(",", "(", ")")
    var result: MutableList<String> = mutableListOf()
    for(param in listParam){
        if (param != "" && param != "range")
            result.add(param)
    }
    return result
}
data class For(
    val exprlist: String,
    val testlist: String,
    val suite: String
) : PlusCalStatement {
    override fun toPlusCal(): String {
        var variable: String = exprlist
        var maxValue: String = "mistake"
        var initValue: String = "0"
        var step: String = "1"

        val list = splitStringFor(testlist)
        if (list.size == 1){
            maxValue = list[0]
        }
        else if (list.size == 2){
            initValue = list[0]
            maxValue = list[1]
        }
        else if (list.size == 3){
            initValue = list[0]
            maxValue = list[1]
            step = list[2]
        }
        return variable + " := " + initValue + ";\n" +
                "while " + variable + " < " + maxValue + " do\n" +
                    suite.addTabs() +
                    (variable + " := " + variable + " + " + step + ";\n").addTabs() +
                "end while;"

        /*"""
            |$variable := $initValue;
            |while $variable < $maxValue;
            |   ${suite.addTabs()}
            |   $variable := $variable + $step;
            |end while
             """.trimMargin()*/
    }
}
/*data class For(
    val beginValue: ForBegin,
    val bodies: List<PlusCalStatement> = listOf()
) : PlusCalStatement {
    override fun toPlusCal(): String {
        val variable: String = beginValue.variable;
        val maxValue: String = beginValue.maxValue;
        val initValue: String = beginValue.initValue;
        val step: String = beginValue.step;
        return """
            |$variable := $initValue
            |while $variable < $maxValue
            |   ${bodies.joinToString("\n") { it.toPlusCal() }}
            |   $variable := $variable + $step
            |end while
             """.trimMargin()
    }
}*/
data class ToWhile(
    val test: PlusCalStatement,
    val suite: PlusCalStatement
) : PlusCalStatement {
    override fun toPlusCal(): String {

        return "while " + test.toPlusCal() + " do\n" +
                    suite.toPlusCal().addTabs() +
                "end while;"
        /*"""
            |while ${test.toPlusCal()}
            |   ${suite.toPlusCal()}
            |end while
             """.trimMargin()*/
    }
}
/*data class ForBegin(
    val variable: String,
    val maxValue: String,
    val initValue: String = "0",
    val step: String = "1",
) : PlusCalStatement {}

data class Expression(
    val variable: Name,
    val num: Name
) : PlusCalStatement {
    override fun toPlusCal(): String {
        return """
            |${variable.toPlusCal()} := ${num.toPlusCal()};
             """.trimMargin()
    }
}*/

data class Comparison(
    val lhs: PlusCalStatement,
    val sign: PlusCalStatement,
    val rhs: PlusCalStatement
    //val listExpr: List<PlusCalStatement>
) : PlusCalStatement {
    override fun toPlusCal(): String {
        var str: String
        if (lhs != sign)
            str = lhs.toPlusCal() + " " + sign.toPlusCal() + " " + rhs.toPlusCal()
        else
            str = lhs.toPlusCal()
        str = str.replace("!=", "/=")
        str = str.replace("==", "=")
        return str
    }
}
/*data class Comparison(
    val lhs: Name,
    val sign: Name,
    val rhs: Name
) : PlusCalStatement {
    override fun toPlusCal(): String {
        return lhs.toPlusCal() + " " + sign.toPlusCal() + " " + rhs.toPlusCal()
    }
}
*/

data class LogicalExpression(
    val listExpr: List<PlusCalStatement>
) : PlusCalStatement {
    override fun toPlusCal(): String {
        var str = ""
        str += """${listExpr[0].toPlusCal()}"""
        for (i in 1 until listExpr.size){
            str += """ and ${listExpr[i].toPlusCal()}"""
        }
        return str
    }
}

data class Name(
    val variable: String
) : PlusCalStatement {
    override fun toPlusCal(): String {
        return variable
    }
}
data class PointName(
    val variable: String
) : PlusCalStatement {
    override fun toPlusCal(): String {
        return "." + variable
    }
}

data class ToOrTest(
    val listOrTest: List<PlusCalStatement>
): PlusCalStatement {
    override fun toPlusCal(): String{
        var str = listOrTest[0].toPlusCal();
        for (i in 1..listOrTest.size - 1){
            str += " or " + listOrTest[i].toPlusCal()
        }
        return str
    }
}

data class ToAndTest(
    val listOrTest: List<PlusCalStatement>
): PlusCalStatement {
    override fun toPlusCal(): String{
        var str = listOrTest[0].toPlusCal();
        for (i in 1..listOrTest.size - 1){
            str += " and " + listOrTest[i].toPlusCal()
        }
        return str
    }
}
data class ToNotTest(
    var comparrison : PlusCalStatement,
    var i : Int
): PlusCalStatement {
    override fun toPlusCal(): String {
        var str = ""
        for (index in 0..i-1){
            str += "not "
        }
        str += comparrison.toPlusCal()
        return str
    }
}
data class ToExpr(
    val listExpr: List<PlusCalStatement>
): PlusCalStatement {
    override fun toPlusCal(): String{
        var str = listExpr[0].toPlusCal();
        for (i in 1..listExpr.size - 1){
            str += " | " + listExpr[i].toPlusCal()
        }
        return str
    }
}
data class ToXorExpr(
    val listExpr: List<PlusCalStatement>
): PlusCalStatement {
    override fun toPlusCal(): String{
        var str = listExpr[0].toPlusCal();
        for (i in 1..listExpr.size - 1){
            str += " ^ " + listExpr[i].toPlusCal()
        }
        return str
    }
}
data class ToAndExpr(
    val listExpr: List<PlusCalStatement>
): PlusCalStatement {
    override fun toPlusCal(): String{
        var str = listExpr[0].toPlusCal();
        for (i in 1..listExpr.size - 1){
            str += " & " + listExpr[i].toPlusCal()
        }
        return str
    }
}
data class ToShiftExpr(
    val listExpr: List<PlusCalStatement>,
    val sign: String
): PlusCalStatement {
    override fun toPlusCal(): String{
        var str = listExpr[0].toPlusCal();
        for (i in 1..listExpr.size - 1){
            str += " " + sign + " " + listExpr[i].toPlusCal()
        }
        return str
    }
}
data class ToArithExpr(
    val listExpr: List<PlusCalStatement>,
    val listOperation: List<String>
    //val sign: String
): PlusCalStatement {
    override fun toPlusCal(): String {
        var str = listExpr[0].toPlusCal();
        for (i in 1..listExpr.size - 1) {
            str += " " + listOperation[i-1] + " " + listExpr[i].toPlusCal()
        }
        return str
    }
}
data class ToTerm(
    val listExpr: List<PlusCalStatement>,
    val listOperation: List<String>
): PlusCalStatement {
    override fun toPlusCal(): String {
        var str = listExpr[0].toPlusCal();
        for (i in 1..listExpr.size - 1) {
            str += " " + listOperation[i-1] + " " + listExpr[i].toPlusCal()
        }
        return str
    }
}
data class ToFactor(
    val expr : PlusCalStatement,
    val sign : String
): PlusCalStatement {
    override fun toPlusCal(): String {
        var str = expr.toPlusCal() + " " + sign;
        return str
    }
}
data class ToAtom_expr(
    val atom : PlusCalStatement,
    val listTrailer: List<PlusCalStatement>
): PlusCalStatement {
    override fun toPlusCal(): String {
        var str = atom.toPlusCal();
        for (i in 0..listTrailer.size-1){
            if (listTrailer[0].toPlusCal()[0] == '.'){
                str += listTrailer[i].toPlusCal();
            }else
                str += " " + listTrailer[i].toPlusCal();
        }
        if (str.contains(".get", ignoreCase = false)){
            str = str.replace(".get(", "[")
            str = str.replace("tuple ((", "<<")
            str = str.replace("))", ">>")
            str = str.replace(")", "]")
        }
        if (str.contains(".add")){
            str = str.replace(".add", " \\union ")
            str = str.replace("${atom.toPlusCal()}", "${atom.toPlusCal()} := ${atom.toPlusCal()}")
            str = str.replace("(", "{")
            str = str.replace(")", "}")
        }
        if (str.contains(".remove")){
            str = str.replace(".remove", " \\")
            str = str.replace("${atom.toPlusCal()}", "${atom.toPlusCal()} := ${atom.toPlusCal()}")
            str = str.replace("(", "{")
            str = str.replace(")", "}")
        }
        if (str.contains(".difference")){
            str = str.replace(".difference(", " \\ ")
            str = str.replace("${atom.toPlusCal()}", "${atom.toPlusCal()} := ${atom.toPlusCal()}")
            str = str.replace(")", "")
        }
        if (str.contains("set ()")){
            str = str.replace("set ()", "{}")
        }
        return str
    }
}

data class ToAtomTestlist_comp(
    val atom : PlusCalStatement,
    val paren : Int
): PlusCalStatement {
    override fun toPlusCal(): String {
        if (paren == 1)
            return "(" + atom.toPlusCal() + ")"
        //if (paren == 2)
        return "[" + atom.toPlusCal() + "]"
    }
}
data class ToTestlist_comp(
    val listTest: List<PlusCalStatement>
): PlusCalStatement {
    override fun toPlusCal(): String {
        var str = listTest[0].toPlusCal()
        for (i in 1..listTest.size - 1){
            str += ", " + listTest[i].toPlusCal();
        }
        return str

    }
}

data class ToComp_for(
    val exprlist: PlusCalStatement,
    val or_test: PlusCalStatement,
    val comp_iter: String
): PlusCalStatement {
    override fun toPlusCal(): String {
        var str = "for " + exprlist.toPlusCal() + " in " + or_test.toPlusCal() + " " + comp_iter
        return str
    }
}

data class ToArglist(
    val listArglist: List<PlusCalStatement>
): PlusCalStatement {
    override fun toPlusCal(): String {
        var str = listArglist[0].toPlusCal();
        for (i in 1..listArglist.size-1){
            str += ", " + listArglist[i].toPlusCal();
        }
        return str
    }
}
data class ToTrailerArg(
    val arglist: String
): PlusCalStatement {
    override fun toPlusCal(): String {
        if (arglist == "")
            return "()"
        return "(" + arglist + ")"
    }
}
data class ToTrailerSub(
    val subscriptlist: PlusCalStatement
): PlusCalStatement {
    override fun toPlusCal(): String {
        return "[" + subscriptlist.toPlusCal() + "]"
    }
}
data class ToSubscriptlist(
    val listSubscriptlist: List<PlusCalStatement>
): PlusCalStatement {
    override fun toPlusCal(): String {
        var str = listSubscriptlist[0].toPlusCal();
        for (i in 1..listSubscriptlist.size-1){
            str += ", " + listSubscriptlist[i].toPlusCal();
        }
        return str
    }
}
//FOR
data class ToExprlist(
    val exprlist: List<PlusCalStatement>
): PlusCalStatement {
    override fun toPlusCal(): String {
        var str = exprlist[0].toPlusCal();
        for (i in 1..exprlist.size-1){
            str += ", " + exprlist[i].toPlusCal();
        }
        return str
    }
}
data class ToStar_expr(
    val expr: PlusCalStatement
): PlusCalStatement {
    override fun toPlusCal(): String {
        return "* " + expr.toPlusCal()
    }
}
data class ToTestlist(
    val testlist: List<PlusCalStatement>
): PlusCalStatement {
    override fun toPlusCal(): String {
        var str = testlist[0].toPlusCal();
        for (i in 1..testlist.size-1){
            str += ", " + testlist[i].toPlusCal();
        }
        return str
    }
}

data class Suite(
    val listStmt: List<PlusCalStatement>
): PlusCalStatement {
    override fun toPlusCal(): String {
        var str = ""
        for (stmt in listStmt) {
            str += stmt.toPlusCal()
        }
        return str
    }
}

fun String.addTabs() = tab + this.replace(Regex("\n"), "\n$tab").dropLast(tab.length)

data class If(
    val test: List<PlusCalStatement>,//condition
    val suite: List<PlusCalStatement>//body
) : PlusCalStatement {
    override fun toPlusCal(): String {
        //var s = suite[0].toPlusCal()
        var str = "if " + test[0].toPlusCal() + " then" + "\n" +
                suite[0].toPlusCal().addTabs()
            /*"""
            |if ${test[0].toPlusCal()} then
            |   ${suite[0].toPlusCal()}
            |
            """.trimMargin()*/
        var i = 1
        val testSize = test.size
        val suiteSize = suite.size
        while (i < testSize) {
            str += "elif ${test[i].toPlusCal()} then\n" + suite[i].toPlusCal().addTabs()
            i++
        }
        if (testSize < suiteSize) {
            str += "else\n" + suite[i].toPlusCal().addTabs();
        }
        str += "end if;"
        return str
    }
}
data class Function(
    val name : String,
    val params : String,
    val suite: String
) : PlusCalStatement {
    override fun toPlusCal(): String{
        if (name == "__init__"){
            var str = "variables " + suite;
            str = str.replace(";", ",")
            str = str.replace(":=", "=")
            if (str[str.lastIndex] == '\n' && str[str.lastIndex-1] == ','){
                str = str.dropLast(2)
                str += ";\n"
            }
            return str
        }
        var str = name + ":" + "\n" + suite.addTabs()
        return str
    }
}
//для обычной функции TLA
/*data class Function(
    val name : String,
    val params : String,
    val suite: String
) : PlusCalStatement {
    override fun toPlusCal(): String{
        var str = """
            |${name}${params} ==
            |${suite}
            |
            """.trimMargin()
        return str
    }
}*/
data class ToParameters(
    val param: String
): PlusCalStatement {
    override fun toPlusCal(): String{
        return "(" + param + ")"
    }
}
data class ListParam(
    val params: List<PlusCalStatement>
): PlusCalStatement {
    override fun toPlusCal(): String{
        var str = "";
        if (params.size > 1){
            for (i in 0 until params.size-1){
                str += params[i].toPlusCal() + ", ";
            }
        }
        return str + params[params.size-1].toPlusCal();
    }
}
data class ToExpressionAnnassign(
    val testlist_star_expr: PlusCalStatement,
    val annassign: PlusCalStatement
): PlusCalStatement {
    override fun toPlusCal(): String{
        return testlist_star_expr.toPlusCal() + " " + annassign.toPlusCal();
    }
}
data class ToExpressionAugassign(
    val testlist_star_expr: PlusCalStatement,
    val augassign: PlusCalStatement,
    val testlist: PlusCalStatement
): PlusCalStatement {
    override fun toPlusCal(): String{
        val sign = augassign.toPlusCal()
        return testlist_star_expr.toPlusCal() + " :" + sign[sign.length-1] + " " + testlist_star_expr.toPlusCal() +
                " " + sign.substring(0, sign.length-1) + " " + testlist.toPlusCal();
    }
}
data class ToExpression(
    val testlist_star_expr: List<PlusCalStatement>
): PlusCalStatement {
    override fun toPlusCal(): String{
        var str = testlist_star_expr[0].toPlusCal()
        for (i in 1..testlist_star_expr.size-1){
            str += " := " + testlist_star_expr[i].toPlusCal();
        }
        return str;
    }
}
data class ToAnnassign(
    val list: List<PlusCalStatement>
): PlusCalStatement {
    override fun toPlusCal(): String {
        var str = ": " + list[0].toPlusCal()
        for (i in 1..list.size - 1) {
            str += " := " + list[i].toPlusCal();
        }
        return str;
    }
}
data class ToAugassign(
    val el: String
): PlusCalStatement {
    override fun toPlusCal(): String{
        return el;
    }
}
data class ToClassdef(
    val name: String,
    val el: String,
    val suite: String
): PlusCalStatement {
    override fun toPlusCal(): String{
        if (el != "")
            return name + "(" + el + "):" + "\n" + suite.addTabs()
        return name +  ":" + "\n" + suite.addTabs()
    }
}

data class ToStmt(
    val expr: PlusCalStatement
): PlusCalStatement {
    override fun toPlusCal(): String{
        var str = expr.toPlusCal() + "\n"
        str = str.replace("\n\n", "\n")
        return str
    }
}
data class ToSimple_stmt(
    val expr: PlusCalStatement
): PlusCalStatement {
    override fun toPlusCal(): String{
        return expr.toPlusCal() + ";"
    }
}
/*1
if z&10:
    1
elif z<10:
    c>5
elif z<100:
    c>50
else:
    c<6
 */
/*2
def func (a, b):
    for i in range (10):
        for j in range (1, 10, 2):
            if z&10:
                1
            elif z<10:
                c>5
if z<100:
        c>50
    else:
        c<6
a > 5
 */
/*3
class name(a, v):
    i = 1234;
    def func(a, b,c):
        a = 2
 */
/*4
class Trader:
    def __init__(self):
        profit = 0
        backpack = set()

    def buy(vendor, item, market):
        if item not in self.backpack:
            profit -= market.get(tuple((vendor, item))).sell
            backpack.add(item)

    def sell(vendor, item, market):
        if item in self.backpack:
            profit += market.get(tuple((vendor, item))).buy
            backpack.remove(item)

    def trade(items, trades):
        backpack.difference(items)
        backpack.add(trades[items])

 */

/*5
def buy(vendor, item, market):
    if item not in self.backpack:
        profit -= market.get(tuple((vendor, item))).sell
        backpack.add(item)
    elif item in self.backpack:
        profit += market.get(tuple((vendor, item))).sell
        backpack.remove(item)
    else:
        profit = 0
        profit *= 2

 */