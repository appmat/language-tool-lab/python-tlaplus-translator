
import kotlin.Throws
import kotlin.jvm.JvmStatic
import java.io.FileInputStream
import org.antlr.v4.runtime.tree.ParseTreeWalker
import org.antlr.v4.runtime.ANTLRInputStream
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.tree.ParseTree
import java.lang.Exception
import java.util.*
import Python3Parser.*
import org.antlr.v4.runtime.CharStreams

object TestPropertyFile {
    @Throws(Exception::class)
    @JvmStatic
    fun main(args: Array<String>) {
        val inputFile = "t.properties"
        var `is` = System.`in`
        if (inputFile != null) {
            `is` = FileInputStream(inputFile)
        }
        val input = ANTLRInputStream(`is`)
        var it = Translator()
        println(it.Translate(input.toString()))
    }
    class Translator {
        fun Translate(input: String): String {
            val lexer = Python3Lexer(CharStreams.fromString(input))
            val tokens = CommonTokenStream(lexer)
            val parser = Python3Parser(tokens)
            val tree: ParseTree = parser.file_input()
            // create a standard ANTLR parse tree walker
            val walker = ParseTreeWalker()
            // create listener then feed to walker
            val loader = PropertyFileLoader()
            walker.walk(loader, tree) // walk parse tree
            loader.statementStack.reverse()
            var output = ""
            while (!loader.statementStack.empty()) {
                output += loader.statementStack.pop().toPlusCal()// print results
            }
            return output
        }
    }
    class PropertyFileLoader : Python3BaseListener() {
        val statementStack: Stack<PlusCalStatement> = Stack()
        val operationStack: Stack<String> = Stack()

        //FOR
        override fun exitFor_stmt(ctx: For_stmtContext) {
            //for_stmt: 'for' exprlist 'in' testlist ':' suite ('else' ':' suite)?;
            //else  в теле цикла не рассматриваю
            val suite = statementStack.pop()
            val testlist = statementStack.pop()
            val exprlist = statementStack.pop()
            statementStack.push(For(exprlist.toPlusCal(), testlist.toPlusCal(), suite.toPlusCal()))
        }

        //WHILE
        override fun enterWhile_stmt(ctx: While_stmtContext?) {
            statementStack.push(WhileBegin)
        }

        override fun exitWhile_stmt(ctx: While_stmtContext?) {
            //while_stmt: 'while' test ':' suite ('else' ':' suite)?;
            //else не рассматриваю, что то уж специфичное
            val suite = statementStack.pop()
            val test = statementStack.pop()
            val el = statementStack.pop()//begin
            if (el !is WhileBegin)
                error("$el is not WhileBegin")
            statementStack.push(ToWhile(test, suite))

        }
        //IF
        override fun enterIf_stmt(ctx: If_stmtContext) {
            statementStack.push(IfBegin)
        }

        override fun exitIf_stmt(ctx: If_stmtContext) {
            //if_stmt: 'if' test ':' suite ('elif' test ':' suite)* ('else' ':' suite)?;
            val listTest: MutableList<PlusCalStatement> = mutableListOf()
            val listSuite: MutableList<PlusCalStatement> = mutableListOf()
            var el = statementStack.pop()
            while (el !is IfBegin){
                if (el is Suite)
                    listSuite.add(el)
                if (el is ToOrTest)
                    listTest.add(el)
                el = statementStack.pop()
            }
            listTest.reverse()
            listSuite.reverse()
            statementStack.push(If(listTest, listSuite))
        }

        override fun enterTest(ctx: TestContext?) {
            //statementStack.push(TestBegin)
        }
        override fun exitTest(ctx: TestContext) {
            //test: or_test ('if' or_test 'else' test)? | lambdef;
            //условие в условие пока не рассматриваем

            /*if (!operationStack.isEmpty()) {
                var elementOne = statementStack.pop()
                val listComparison: MutableList<Comparison> = mutableListOf()
                while (!operationStack.isEmpty()) {
                    var sign = operationStack.pop()
                    var elementTwo = statementStack.pop()
                    //listComparison.add(Comparison(elementTwo as Name, Name(sign), elementOne as Name))
                    elementOne = elementTwo
                }
                statementStack.push(LogicalExpression(listComparison))
            }*/
        }

        override fun enterOr_test(ctx: Or_testContext?) {
            statementStack.push(Or_testBegin)
        }
        override fun exitOr_test(ctx: Or_testContext?) {
            //or_test: and_test ('or' and_test)*;
            var listTest: MutableList<PlusCalStatement> = mutableListOf()
            var el = statementStack.pop()
            while (el !is Or_testBegin){
                listTest.add(el)
                el = statementStack.pop()
            }
            listTest.reverse()
            statementStack.push(ToOrTest(listTest))
        }

        override fun enterAnd_test(ctx: And_testContext?) {
            statementStack.push(And_testBegin)
        }
        override fun exitAnd_test(ctx: And_testContext?) {
            //and_test: not_test ('and' not_test)*;
            var listTest: MutableList<PlusCalStatement> = mutableListOf()
            var el = statementStack.pop()
            while (el !is And_testBegin){
                listTest.add(el)
                el = statementStack.pop()
            }
            listTest.reverse()
            statementStack.push(ToAndTest(listTest))
        }

        override fun enterNot_test(ctx: Not_testContext?) {
            statementStack.push(Not_testBegin)
        }
        override fun exitNot_test(ctx: Not_testContext?) {
            //not_test: 'not' not_test | comparison;
            var i = 0;
            var elBegin = statementStack.pop()
            var el = elBegin
            while (elBegin !is Not_testBegin){
                el = elBegin
                i++;
                elBegin = statementStack.pop()
            }
            statementStack.push(ToNotTest(el, i-1))
        }

        override fun enterComparison(ctx: ComparisonContext?) {
            statementStack.push(ComparisonBegin)
        }
        override fun exitComparison(ctx: ComparisonContext?) {
            //comparison: expr (comp_op expr)*;
            var listExpr: MutableList<PlusCalStatement> = mutableListOf()
            var rhs = statementStack.pop()
            var sign = statementStack.pop()
            var lhs: PlusCalStatement
            if (sign !is ComparisonBegin){
                lhs = statementStack.pop()
                statementStack.pop() //CompBegin
                statementStack.push(Comparison(lhs, sign, rhs))
            }else{
                statementStack.push(Comparison(rhs, rhs ,rhs))
            }
            /*el = statementStack.pop()
            while (el !is ComparisonBegin){
                listExpr.add(el)
                el = statementStack.pop()
            }
            listExpr.reverse()
            statementStack.push(Comparison(listExpr))*/
        }

        /*override fun enterComp_op(ctx: Comp_opContext) {
            statementStack.push(Comp_opBegin)
        }*/
        override fun exitComp_op(ctx: Comp_opContext) {
            if (!ctx.isEmpty) {
                statementStack.push(Name(ctx.getChild(0).toString()))
                //operationStack.push(ctx.getChild(0).toString())
            }
        }

        override fun enterExpr(ctx: ExprContext?) {
            statementStack.push(ExprBegin)
        }
        override fun exitExpr(ctx: ExprContext?) {
            //expr: xor_expr ('|' xor_expr)*;
            var listExpr: MutableList<PlusCalStatement> = mutableListOf()
            var el = statementStack.pop()
            while (el !is ExprBegin){
                listExpr.add(el)
                el = statementStack.pop()
            }
            listExpr.reverse()
            statementStack.push(ToExpr(listExpr))
        }

        override fun enterXor_expr(ctx: Xor_exprContext?) {
            statementStack.push(Xor_exprBegin)
        }
        override fun exitXor_expr(ctx: Xor_exprContext?) {
            //xor_expr: and_expr ('^' and_expr)*;
            var listExpr: MutableList<PlusCalStatement> = mutableListOf()
            var el = statementStack.pop()
            while (el !is Xor_exprBegin){
                listExpr.add(el)
                el = statementStack.pop()
            }
            listExpr.reverse()
            statementStack.push(ToXorExpr(listExpr))
        }

        override fun enterAnd_expr(ctx: And_exprContext?) {
            statementStack.push(And_exprBegin)
        }
        override fun exitAnd_expr(ctx: And_exprContext?) {
            //and_expr: shift_expr ('&' shift_expr)*;
            var listExpr: MutableList<PlusCalStatement> = mutableListOf()
            var el = statementStack.pop()
            while (el !is And_exprBegin){
                listExpr.add(el)
                el = statementStack.pop()
            }
            listExpr.reverse()
            statementStack.push(ToAndExpr(listExpr))
        }

        override fun enterShift_expr(ctx: Shift_exprContext?) {
            statementStack.push(Shift_exprBegin)
        }

        override fun exitShift_expr(ctx: Shift_exprContext) {
            //shift_expr: arith_expr (('<<'|'>>') arith_expr)*;
            var listExpr: MutableList<PlusCalStatement> = mutableListOf()
            var el = statementStack.pop()
            //var sign  = ctx.getChild(0).toString()
            while (el !is Shift_exprBegin){
                listExpr.add(el)
                el = statementStack.pop()
            }
            listExpr.reverse()
            statementStack.push(ToShiftExpr(listExpr, ""))
        }

        override fun enterArith_expr(ctx: Arith_exprContext?) {
            statementStack.push(Arith_exprBegin)
        }
        override fun exitArith_expr(ctx: Arith_exprContext) {
            //arith_expr: term (('+'|'-') term)*;
            var listExpr: MutableList<PlusCalStatement> = mutableListOf()
            var listOperation: MutableList<String> = mutableListOf()
            var el = statementStack.pop()
            //var sign = ""
            if (ctx.childCount > 1)
                for (i in 1..ctx.childCount-1 step 2){
                    listOperation.add(ctx.getChild(i).toString())
                }
                //sign  = ctx.getChild(1).toString()
            while (el !is Arith_exprBegin){
                listExpr.add(el)
                el = statementStack.pop()
            }
            listExpr.reverse()
            statementStack.push(ToArithExpr(listExpr, listOperation))
        }

        override fun enterTerm(ctx: TermContext) {
            statementStack.push(TermBegin)
        }
        override fun exitTerm(ctx: TermContext) {
            //term: factor (('*'|'@'|'/'|'%'|'//') factor)*;
            var listExpr: MutableList<PlusCalStatement> = mutableListOf()
            var listOperation: MutableList<String> = mutableListOf()
            var el = statementStack.pop()
            //var sign  = ctx.getChild(1).toString()
            if (ctx.childCount > 1)
                for (i in 1..ctx.childCount-1 step 2){
                    listOperation.add(ctx.getChild(i).toString())
                }
            while (el !is TermBegin){
                listExpr.add(el)
                el = statementStack.pop()
            }
            listExpr.reverse()
            statementStack.push(ToTerm(listExpr, listOperation))
        }

        /*override fun enterFactor(ctx: FactorContext) {
            statementStack.push(FactorBegin)
        }
        override fun exitFactor(ctx: FactorContext) {
            //factor: ('+'|'-'|'~') factor | power;
            //НЕ ЗНАЮ КАК ДОСТАТЬ ЗНАК и надо ли вообще
            //var sign = ctx.getChild(0).toString()
            var elBegin = statementStack.pop()
            var el = elBegin
            while (elBegin !is FactorBegin){
                el = elBegin
                //i++;
                elBegin = statementStack.pop()
            }
            statementStack.push(ToFactor(el, sign))
        }*/

        override fun enterPower(ctx: PowerContext) {
            //statementStack.push(PowerBegin)
        }

        override fun exitPower(ctx: PowerContext) {
            //power: atom_expr ('**' factor)?;
            //** такое не рассматриваю пока
        }

        override fun enterAtom_expr(ctx: Atom_exprContext) {
            statementStack.push(Atom_exprBegin)
        }

        override fun exitAtom_expr(ctx: Atom_exprContext) {
            //atom_expr: (AWAIT)? atom trailer*;
            //AWAIT пока не проверяю
            var atom = statementStack.pop()
            var listTrailer: MutableList<PlusCalStatement> = mutableListOf()
            var begin = statementStack.pop()
            if (begin is Atom_exprBegin)
                statementStack.push(ToAtom_expr(atom, listTrailer))
            else{
                var trailer = atom
                atom = begin
                while (atom !is Atom_exprBegin){
                    listTrailer.add(trailer)
                    trailer = atom
                    atom = statementStack.pop()
                }
                atom = trailer
                listTrailer.reverse()
                statementStack.push(ToAtom_expr(atom, listTrailer))
            }
            /*
            var atom = statementStack.pop()
            var listTrailer: MutableList<PlusCalStatement> = mutableListOf()
            var trailer = statementStack.pop()
            while (trailer !is Atom_exprBegin){
                trailer = atom
                listTrailer.add(trailer)

                trailer = statementStack.pop()
            }
            statementStack.push(ToAtom_expr(atom, listTrailer))*/
        }

        override fun enterAtom(ctx: AtomContext) {
            statementStack.push(AtomBegin);
        }
        override fun exitAtom(ctx: AtomContext) {
            //atom: ('(' (yield_expr|testlist_comp)? ')' |
            //       '[' (testlist_comp)? ']' |
            //       '{' (dictorsetmaker)? '}' |
            //       NAME | NUMBER | STRING+ | '...' | 'None' | 'True' | 'False');
            //допустим что testlist_comp только в таких скобках []
            var paren = 0
            if (ctx.getChild(0).toString() == "("){
                paren = 1
            }else if (ctx.getChild(0).toString() == "[") {
                paren = 2
            }
            var el = statementStack.pop()
            if (el !is AtomBegin){
                val begin = statementStack.pop() //AtomBegin
                if (el is ToTestlist_comp){
                    statementStack.push(ToAtomTestlist_comp(el, paren))
                }
                else {
                    error("$el нужно обработать")
                }

                check(begin is AtomBegin) {
                    error("$begin is not AtomBegin")
                }
                //else выражение в круглых скобках
            }
            else {

                if (ctx.NAME() != null) {
                    //&& ctx.NAME().toString() != "range") {
                    statementStack.push(Name(ctx.NAME().toString()))
                } else if (ctx.NUMBER() != null) {
                    statementStack.push(Name(ctx.NUMBER().toString()))
                } else if (ctx.STRING() != null) {
                    statementStack.push(Name(ctx.STRING().toString()))
                }
            }
        }

        override fun enterTestlist_comp(ctx: Testlist_compContext?) {
            statementStack.push(Testlist_compBegin);
        }

        override fun exitTestlist_comp(ctx: Testlist_compContext?) {
            //testlist_comp: (test|star_expr) ( comp_for | (',' (test|star_expr))* (',')? );
            var listTest: MutableList<PlusCalStatement> = mutableListOf()
            val comp_for = statementStack.pop()
            var test: PlusCalStatement
            if (comp_for is ToComp_for){
                listTest.add(comp_for)
                test = statementStack.pop()
                listTest.add(test)
                test = statementStack.pop()//begin
            }else{
                test = comp_for
                while (test !is Testlist_compBegin){
                    listTest.add(test)
                    test = statementStack.pop()
                }
            }
            if (test !is Testlist_compBegin)
                error("$test is not Testlist_compBegin")
            listTest.reverse()
            statementStack.push(ToTestlist_comp(listTest))
            /*var test = statementStack.pop()
            var listTest: MutableList<PlusCalStatement> = mutableListOf()
            //if (test is TestContext || test is Star_exprContext){
            var el = statementStack.pop()
            if (el is ToComp_for){
                listTest.add(el)
                el = statementStack.pop()
                if (el !is Testlist_compBegin)
                    error("$el is not Testlist_compBegin")
            }
            else{
                while (el !is Testlist_compBegin){
                    listTest.add(el)
                    el = statementStack.pop()
                }
            }
            listTest.reverse()
            statementStack.push(ToTestlist_comp(test, listTest))*/
        }

        override fun enterComp_for(ctx: Comp_forContext?) {
            statementStack.push(Comp_forBegin)
        }

        override fun exitComp_for(ctx: Comp_forContext?) {
            //comp_for: (ASYNC)? 'for' exprlist 'in' or_test (comp_iter)?;
            //(ASYNC)? не рассматриваю асинхронность
            var el = statementStack.pop()
            val or_test = statementStack.pop()
            val exprlist = statementStack.pop()
            if (exprlist !is Comp_forBegin){
                val begin = statementStack.pop()
                if (begin !is Comp_forBegin)
                    error("$begin is not Comp_forBegin")
                statementStack.push(ToComp_for(exprlist, or_test, el.toPlusCal()))
            }else{
                if (exprlist !is Comp_forBegin)
                    error("$exprlist is not Comp_forBegin")
                statementStack.push(ToComp_for(or_test, el, ""))
            }
        }

        override fun enterComp_iter(ctx: Comp_iterContext?) {

        }
        override fun exitComp_iter(ctx: Comp_iterContext?) {
            //comp_iter: comp_for | comp_if;
            //ничего важного здесь не происходит, можно не обрабатывать
        }

        override fun enterTrailer(ctx: TrailerContext) {
            statementStack.push(TrailerBegin);
        }
        override fun exitTrailer(ctx: TrailerContext) {
            //trailer: '(' (arglist)? ')' | '[' subscriptlist ']' | '.' NAME;
            var el = statementStack.pop()
            if (el is ToSubscriptlist){
                statementStack.pop()//begin
                statementStack.push(ToTrailerSub(el))
            }
            else if (el is ToArglist){
                statementStack.pop()//begin
                statementStack.push(ToTrailerArg(el.toPlusCal()))
            }else if (ctx.NAME() != null){
                //el is begin
                statementStack.push(PointName(ctx.NAME().toString()))
            }else if (el is TrailerBegin){
                statementStack.push(ToTrailerArg(""))
            }
            //
            /*var el = statementStack.pop()
            val begin = statementStack.pop() // TrailerBegin
            if (begin !is TrailerBegin)
                error("$begin is not TrailerBegin")
            if (el is ToArglist)
                statementStack.push(ToTrailerArg(el))
            else if (el is ToSubscriptlist)
                statementStack.push(ToTrailerSub(el))
            else {
                statementStack.push(el)
                statementStack.push(PointName(ctx.NAME().toString())) //скорее всего это для вызова метода массива/списка и прочего
            }*/
        }

        override fun enterArglist(ctx: ArglistContext) {
            statementStack.push(ArglistBegin);
        }
        override fun exitArglist(ctx: ArglistContext) {
            //arglist: argument (',' argument)*  (',')?;
            //запятая в конце это странно, пока не рассматриваю
            var el = statementStack.pop()
            var listArglist: MutableList<PlusCalStatement> = mutableListOf()
            while (el !is ArglistBegin){
                listArglist.add(el)
                el = statementStack.pop()
            }
            listArglist.reverse()
            statementStack.push(ToArglist(listArglist))
        }

        override fun enterSubscriptlist(ctx: SubscriptlistContext?) {
            statementStack.push(SubscriptlistBegin);
        }
        override fun exitSubscriptlist(ctx: SubscriptlistContext?) {
            //subscriptlist: subscript (',' subscript)* (',')?;
            var el = statementStack.pop()
            var listSubscriptlist: MutableList<PlusCalStatement> = mutableListOf()
            while (el !is SubscriptlistBegin){
                listSubscriptlist.add(el)
                el = statementStack.pop()
            }
            statementStack.push(ToSubscriptlist(listSubscriptlist))
        }

        override fun enterSubscript(ctx: SubscriptContext?) {
            //statementStack.push(SubscriptBegin);
        }

        override fun exitSubscript(ctx: SubscriptContext?) {
            //subscript: test | (test)? ':' (test)? (sliceop)?;
            //не понятно для чего
        }

        override fun enterExpr_stmt(ctx: Expr_stmtContext) {
            statementStack.push(Expr_stmtBegin);
        }

        override fun exitExpr_stmt(ctx: Expr_stmtContext) {
            //expr_stmt: testlist_star_expr (annassign | augassign (yield_expr|testlist) |
            //           ('=' (yield_expr|testlist_star_expr))*);
                val annassign = statementStack.pop()
                var testlist_star_expr = statementStack.pop()
                if (annassign is ToAnnassign){
                    val el = statementStack.pop()
                    if (el !is Expr_stmtBegin)
                        error("$el встречен в exitExpr_stmt а ожидался Expr_stmtBegin")
                    statementStack.push(ToExpressionAnnassign(testlist_star_expr, annassign))
                }
                else if (testlist_star_expr is ToAugassign) { // annassign является testlist
                    val testlist = annassign
                    val augassign = testlist_star_expr
                    val testlist_star_expr = statementStack.pop()

                    val el = statementStack.pop()
                    if (el !is Expr_stmtBegin)
                        error("$el встречен в exitExpr_stmt а ожидался Expr_stmtBegin")
                    statementStack.push(ToExpressionAugassign(testlist_star_expr, augassign, testlist))
                }else {
                    var list: MutableList<PlusCalStatement> = mutableListOf()
                    list.add(annassign) // это testlist_star_expr
                    if (testlist_star_expr is Expr_stmtBegin) //только testlist_star_expr
                        statementStack.push(ToExpression(list))
                    else {
                        while (testlist_star_expr !is Expr_stmtBegin) {
                            list.add(testlist_star_expr)
                            testlist_star_expr = statementStack.pop()
                        }
                        list.reverse()
                        statementStack.push(ToExpression(list))
                    }
                }
            //неверно
            /*val num = statementStack.pop()
            val variable = statementStack.pop()
            when (variable) {
                is Expr_stmtBegin -> statementStack.push(num)
                is Name -> {
                    val topStatement = statementStack.pop()
                    check(topStatement is Expr_stmtBegin) { "$topStatement is not Expr_stmtBegin" }
                    check(num is Name) { "$num is not Name" }
                    //statementStack.push(Expression(variable, num))
                }
                else -> error { "$variable is not Expr_stmtBegin or Name" }
            }*/
        }

        override fun enterAnnassign(ctx: AnnassignContext) {
            statementStack.push(AnnassignBegin);
        }
        override fun exitAnnassign(ctx: AnnassignContext) {
            //annassign: ':' test ('=' test)?;
            var test = statementStack.pop()
            var list: MutableList<PlusCalStatement> = mutableListOf()
            while (test !is AnnassignBegin){
                list.add(test)
                test = statementStack.pop()
            }
            list.reverse()
            statementStack.push(ToAnnassign(list))
        }

        override fun enterAugassign(ctx: AugassignContext) {
            //statementStack.push(AugassignBegin);
        }
        override fun exitAugassign(ctx: AugassignContext) {
            statementStack.push(ToAugassign(ctx.getChild(0).toString()))
        }
        //FOR
        override fun enterExprlist(ctx: ExprlistContext) {
            statementStack.push(ExprlistBegin);
        }

        override fun exitExprlist(ctx: ExprlistContext) {
            //exprlist: (expr|star_expr) (',' (expr|star_expr))* (',')?;
            var el = statementStack.pop()
            var exprlist: MutableList<PlusCalStatement> = mutableListOf()
            while (el !is ExprlistBegin) {
                if (el is ToExpr || el is ToStar_expr) {
                    exprlist.add(el)
                    el = statementStack.pop()
                }
                else
                    error("$el встречен в exitExprlist а ожидался ExprlistBegin")
            }
            statementStack.push(ToExprlist(exprlist))
            /*val variable = statementStack.pop()
            check(statementStack.pop() is ExprlistBegin) { error("Many arguments") }
            check(variable is Name) { error("$variable is not Name") }
            statementStack.push(variable)*/
        }

        override fun enterStar_expr(ctx: Star_exprContext?) {
            statementStack.push(Star_exprBegin);
        }

        override fun exitStar_expr(ctx: Star_exprContext?) {
            //star_expr: '*' expr;
            val el = statementStack.pop()
            val begin = statementStack.pop()
            if (begin !is Star_exprBegin)
                error("$begin встречен в exitStar_expr, а ожидался Star_exprBegin")
            statementStack.push(ToStar_expr(el))
        }

        override fun enterTestlist(ctx: TestlistContext) {
            statementStack.push(TestlistBegin);
        }

        override fun exitTestlist(ctx: TestlistContext) {
            //testlist: test (',' test)* (',')?;
            var el = statementStack.pop()
            var testlist: MutableList<PlusCalStatement> = mutableListOf()
            while (el !is TestlistBegin) {
                testlist.add(el)
                el = statementStack.pop()
            }
            testlist.reverse()
            statementStack.push(ToTestlist(testlist))
            /*
            //один атом (for group in groups)
            val atomFirst = statementStack.pop();
            val atomSecond = statementStack.pop();
            if (atomSecond is TestlistBegin)
                statementStack.push(atomFirst);
            //больше одного атома (обыchный for)
            else {
                var arguments: MutableList<PlusCalStatement> = mutableListOf(atomFirst, atomSecond);
                var atom = statementStack.pop();
                while (atom !is TestlistBegin) {
                    arguments.add(atom);
                    atom = statementStack.pop();
                }
                check(arguments.last() == Name("range")) { error("Ожидался range") }
                val variable = statementStack.pop()
                when (arguments.size) {
                    2 -> statementStack.push(ForBegin(variable.toPlusCal(), arguments[0].toPlusCal()))
                    3 -> statementStack.push(
                        ForBegin(
                            variable.toPlusCal(),
                            arguments[0].toPlusCal(),
                            arguments[1].toPlusCal()
                        )
                    )
                    4 -> statementStack.push(
                        ForBegin(
                            variable.toPlusCal(),
                            arguments[1].toPlusCal(),
                            arguments[2].toPlusCal(),
                            arguments[0].toPlusCal()
                        )
                    )
                    else -> error { "Many arguments" }
                }*/
        }



        //функция
        override fun enterFuncdef(ctx: FuncdefContext) {
            statementStack.push(FuncdefBegin)
        }
        override fun exitFuncdef(ctx: FuncdefContext) {
            //funcdef: 'def' NAME parameters ('->' test)? ':' suite;
            var expression = statementStack.pop()
            var params = statementStack.pop()
            var name = ctx.NAME()
            var el = statementStack.pop()
            if (el !is FuncdefBegin)
                return error("Error in Function")
            statementStack.push(Function(name.toString(), params.toPlusCal(),expression.toPlusCal()))
        }
        //параметры для функции
        override fun enterParameters(ctx: ParametersContext) {
            statementStack.push(ParametersBegin)
        }
        override fun exitParameters(ctx: ParametersContext) {
            //parameters: '(' (typedargslist)? ')';
            var el = statementStack.pop()
            if (el is ParametersBegin){
                statementStack.push(ToParameters(""))
            }else{
                statementStack.pop() //begin
                statementStack.push(ToParameters(el.toPlusCal()))
            }
        }
        //содержимое параметров
        override fun enterTypedargslist(ctx: TypedargslistContext?) {
            statementStack.push(TypedargslistBegin)
        }
        override fun exitTypedargslist(ctx: TypedargslistContext) {
            var el = statementStack.pop()
            //сложные конструкции с вложенными условными операторами пока не рассматриваю
            val listParam: MutableList<PlusCalStatement> = mutableListOf()
            while (el !is TypedargslistBegin){
                listParam.add(el);
                el = statementStack.pop()
            }
            listParam.reverse()
            statementStack.push(ListParam(listParam))
        }
        //переменная в параметрах
        override fun enterTfpdef(ctx: TfpdefContext) {
            statementStack.push(TfpdefBegin)
        }
        override fun exitTfpdef(ctx: TfpdefContext) {
            val el = statementStack.pop()
            check(el is TfpdefBegin) { error("$el is not TfpdefBegin")}
            //сложные конструкции с вложенными условными операторами по типу "a: if 2<3: что то там) пока не рассматриваю
            /*if (ctx.test() != null)
                statementStack.push()*/
            //эта строка соответствует такой записи параметров func(a)
            statementStack.push(Name(ctx.NAME().toString()))
        }

        //нормальный Suite(тело функций/циклов и прочее)
        override fun enterSuite(ctx: SuiteContext?) {
            //suite: simple_stmt | NEWLINE INDENT stmt+ DEDENT;
            statementStack.push(SuiteBegin)
        }
        override fun exitSuite(ctx: SuiteContext?) {
            var el = statementStack.pop()
            val listStmt: MutableList<PlusCalStatement> = mutableListOf()
            while (el !is SuiteBegin){
                listStmt.add(el);
                el = statementStack.pop()
            }
            listStmt.reverse()
            statementStack.push(Suite(listStmt))
        }

        override fun enterSimple_stmt(ctx: Simple_stmtContext) {
            //statementStack.push(SimpleStmtBegin)
        }
        override fun exitSimple_stmt(ctx: Simple_stmtContext) {
            //нужно для того, чтобы отделить выражения ;,но я в Suite отделяю , так что это не нужно
            statementStack.push(ToSimple_stmt(statementStack.pop()))
        }

        override fun enterSmall_stmt(ctx: Small_stmtContext?) {
            statementStack.push(SmallStmtBegin)
        }
        override fun exitSmall_stmt(ctx: Small_stmtContext?) {
            //small_stmt: (expr_stmt | del_stmt | pass_stmt | flow_stmt |
            //             import_stmt | global_stmt | nonlocal_stmt | assert_stmt);
            val stmt = statementStack.pop()
            val el = statementStack.pop()
            check(el is SmallStmtBegin) { error("$el is not SmallStmtBegin")}
            statementStack.push(stmt)
        }

        override fun enterCompound_stmt(ctx: Compound_stmtContext) {
            statementStack.push(CompoundStmtBegin);
        }

        override fun exitCompound_stmt(ctx: Compound_stmtContext) {
            //compound_stmt: if_stmt | while_stmt | for_stmt | try_stmt | with_stmt | funcdef | classdef | decorated | async_stmt;
            var stmt = statementStack.pop()
            val el = statementStack.pop()
            check(el is CompoundStmtBegin) { error("$el is not CompoundStmtBegin")}
            statementStack.push(stmt)
        }

        override fun enterClassdef(ctx: ClassdefContext) {
            statementStack.push(ClassdefBegin)
        }

        override fun exitClassdef(ctx: ClassdefContext) {
            //classdef: 'class' NAME ('(' (arglist)? ')')? ':' suite;
            val suite = statementStack.pop()
            val name = ctx.NAME().toString()
            var el = statementStack.pop()
            if (el !is ClassdefBegin) {
                statementStack.pop()//Begin
                statementStack.push(ToClassdef(name, el.toPlusCal(), suite.toPlusCal()))
            }
            else
                statementStack.push(ToClassdef(name, "", suite.toPlusCal()))
        }
        override fun enterStmt(ctx: StmtContext) {
            statementStack.push(StmtBegin);
        }

        override fun exitStmt(ctx: StmtContext) {
            //small_stmt: (expr_stmt | del_stmt | pass_stmt | flow_stmt |
            //             import_stmt | global_stmt | nonlocal_stmt | assert_stmt);
            val el = statementStack.pop();
            val begin = statementStack.pop()
            if (begin !is StmtBegin){
                error("$begin is not StmtBegin")
            }
            statementStack.push(ToStmt(el))
        }

    }
}


